# Tender Lots Details

This extension Adds some fields to the lot with local information of the lots used in Paraguay 
as if the lot is `simultaneousSupply` (boolean),  `openContractType` (as "Por Monto", "Por Cantidad"), 
the `minValue`, the `statusDetails` and the `deliveryAddress` for the lot.

## Example

```json
{
  "tender": {
        "lots": [
            {
                "id": "1",
                "title": "Token y certificado digital",
                "status": "active",
                "statusDetails": "Activo",
                "value": {
                    "amount": 35000000,
                    "currency": "PYG"
                },
                "simultaneousSupply": false,
                
                "openContractType": "Por monto",
                "minValue": {
                    "amount": 17500000,
                    "currency": "PYG"
                },
                "deliveryAddress": {
                    "streetAddress": "Conforme al Anexo C de la Carta de Invitación"
                }
            }
        ]
  }
}
```